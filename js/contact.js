export function writeContact() {
  return `

  <div class="container mt-5 mv-5">
  <br /><br />
    <div class="row">
      <div class="col-xl-10 col-md-8 col-sm-6">      
      <h2 class="display-3">Contact Me</h2>
      <address class="h4 ml-3 mb-4 mt-4">
        1234 Waylay street <br />Downer Avenue BC, Canada <br />(123)
        456-7890
      </address>

      <form class="ml-3" >
        <div class="form-group mt-4 mb-4">
          <label class="h5" for="name">Name:<span style="color: red">*</span></label>
          <input
            class="form-control"
            placeholder="Type your name here"
            type="text"
            required
          />
        </div>
        <div class="form-group">
          <label class="h5" for="name">Email:<span style="color: red">*</span></label>
          <input
            class="form-control"
            placeholder="Type your email here"
            type="email"
            required
          />
        </div>
        <div>
          <button class="btn btn-warning btn-lg btn-block mt-5">Contact</button>
        </div>
      </form>


    </div>
    </div>
  </div>  
`;
}
