export function writeHome() {
  return `

  <div class="jumbotron jumbotron-fluid" style="height: 100vh" id="home-jumbo" >
    <div class="container text-white" >
      <h1 class="display-1 mt-5" style="font-siz: 10rem;" >Karen K</h1>
      <p class="display-3" >Full Stack Developer...</p>
    </div>
  </div>

`;
}