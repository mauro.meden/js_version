export function writeNav() {
  return `
  <!-- navbar -->
  <section id="navbar">
  <nav class="navbar navbar-expand-md navbar-light bg-dark fixed-top">
    <a href="#" class="navbar-brand"><i class="fas fa-rocket text-warning fa-1x"></i></a>

    <button type="button" class="navbar-toggler bg-light" data-toggle="collapse" data-target="#nav"><span class="navbar-toggler-icon"></span></button>

    <div class="collapse navbar-collapse justify-content-between" id="nav">
    <ul class="navbar-nav">
    <li class="nav-item" id="clickHome"><a class="nav-link text-light text-uppercase font-weight-bold px-3" href="#">Home</a></li>
    <li class="nav-item" id="clickResume"><a class="nav-link text-light text-uppercase font-weight-bold px-3" href="#">Resume</a></li>
    <li class="nav-item" id="clickPortfolio" ><a class="nav-link text-light text-uppercase font-weight-bold px-3" href="#">Portfolio</a></li>
    <li class="nav-item" id="clickContact" ><a class="nav-link text-light text-uppercase font-weight-bold px-3" href="#">Contact</a></li>
    </ul>

    <form class="form-inline ml-3">
      <div class="input-group">
        <input class="form-control" type="text" placeholder="search">
        <div class="input-group-append">
          <button class="btn" type="button"><i class="fas fa-search text-muted"></i></button>
        </div>
      </div>
    </form>
    </div>
  </nav>
  <!--end nav-->
</section>
<!-- end of navbar--></section>`

/*
  return `
  <ul>
  <li id="clickContact" style="cursor: pointer">Contact me</li>
  <li id="clickPortfolio" style="cursor: pointer">Portfolio</li>
  <li id="clickResume" style="cursor: pointer">Resume</li>
  </ul>`;
  */
}

