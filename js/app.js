import {writeHeader} from './header.js';
import {writeNav} from './nav.js';
import {writeFooter} from './footer.js';

// nav functions
import {writeContact} from './contact.js';
import {writeHome} from './home.js';
import {writePortfolio} from './portfolio.js';
import {writeResume} from './resume.js';

//document structure
let header = document.getElementById('header');
let nav = document.getElementById('nav');
let body = document.getElementById('body');
let footer = document.getElementById('footer');
header.innerHTML += writeHeader();
nav.innerHTML += writeNav();
footer.innerHTML += writeFooter();


// get elements
const clickContact = document.getElementById('clickContact');
const clickHome = document.getElementById('clickHome');
const clickResume = document.getElementById('clickResume');
const clickPortfolio = document.getElementById('clickPortfolio');

// When browse to index at start
window.onload = () => navTo(writeHome);

// Navigation event handlers
clickHome.addEventListener('click', () => { 
  navTo(writeHome); 
});
clickResume.addEventListener('click', () => { 
console.log('hi');
  navTo(writeResume); 
});
clickPortfolio.addEventListener('click', () => { 
    navTo(writePortfolio); 
  });
clickContact.addEventListener('click', () => { 
    navTo(writeContact); 
  });
  

// functions
function navTo(func) {
  while (body.firstChild) body.removeChild(body.firstChild);
  let div = document.createElement('div');
  body.appendChild(div)
  div.innerHTML += func();
}
